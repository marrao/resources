# xset

## Examples:

```
xset q
```

Change repeat values:

```
xset r rate 200 20
```

will set ```auto repeat delay:  200    repeat rate:  20```
